define("GbcApplicants1Page", [], function() {
	return {
		entitySchemaName: "GbcApplicants",
		attributes: {},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{
			"GbcSchema5ac6dcd0Detaila48a2dc1": {
				"schemaName": "GbcSchema5ac6dcd0Detail",
				"entitySchemaName": "GbcResume",
				"filter": {
					"detailColumn": "GbcApplicant",
					"masterColumn": "Id"
				}
			},
			"ActivityDetailInApplicant": {
				"schemaName": "ActivityDetailV2",
				"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "GbcActivityLookupApplicants",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{
			"GbcStatus": {
				"32220c05-44ab-4115-8a3e-780948d1d292": {
					"uId": "32220c05-44ab-4115-8a3e-780948d1d292",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 0,
					"conditions": [
						{
							"comparisonType": 3,
							"leftExpression": {
								"type": 3,
								"value": "CurrentUserContact",
								"dataValueType": 10
							},
							"rightExpression": {
								"type": 0,
								"value": "410006e1-ca4e-4502-a9ec-e54d922d2c00",
								"dataValueType": 10
							}
						}
					]
				}
			},
			"GbcName": {
				"675771ae-46b8-41f7-a7e9-bbfe0b8765be": {
					"uId": "675771ae-46b8-41f7-a7e9-bbfe0b8765be",
					"enabled": true,
					"removed": false,
					"ruleType": 0,
					"property": 1,
					"logical": 1,
					"conditions": [
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "GbcContact"
							}
						},
						{
							"comparisonType": 1,
							"leftExpression": {
								"type": 1,
								"attribute": "GbcJob"
							}
						}
					]
				}
			}
		}/**SCHEMA_BUSINESS_RULES*/,
		methods: {},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "GbcNameb7f2cfb2-e017-4d24-9bba-dfabb17b9a30",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "GbcName",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUPad1757ce-6af1-47fb-9398-5d1217229d40",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "GbcJob",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "INTEGER4ebc5522-f4ea-41f9-9ac3-04e1db2b8c42",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "GbcExpectedSalary",
					"enabled": true
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "STRINGa8b1bf03-a172-45fe-9496-618248cc0ff3",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "Header"
					},
					"bindTo": "GbcSkills",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "LOOKUP7c4036b9-42e6-4102-a679-5f1b7fbfda14",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "GbcApplicantWishfulVacancy",
					"labelConfig": {
						"caption": {
							"bindTo": "Resources.Strings.LOOKUP7c4036b942e64102a6795f1b7fbfda14LabelCaption"
						}
					},
					"enabled": true,
					"contentType": 5
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "LOOKUP788669d2-20b9-447e-b406-f10460aacd3b",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "Header"
					},
					"bindTo": "GbcStatus",
					"enabled": true,
					"contentType": 3
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "GbcNotes6445083a-ff9d-403b-8689-8b77f24a2e04",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "Header"
					},
					"bindTo": "GbcNotes",
					"enabled": true,
					"contentType": 0
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTab",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabCaption"
					},
					"items": [],
					"order": 0
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTabGroup32b68135",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.NotesAndFilesTabGroup32b68135GroupCaption"
					},
					"itemType": 15,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "NotesAndFilesTabGridLayoutb42c0698",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "NotesAndFilesTabGroup32b68135",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "LOOKUPc4da4964-df8a-47a8-a602-e91d9ff44c08",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "NotesAndFilesTabGridLayoutb42c0698"
					},
					"bindTo": "GbcContact",
					"enabled": true,
					"contentType": 5
				},
				"parentName": "NotesAndFilesTabGridLayoutb42c0698",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "STRING8ea9ce39-74f3-40a3-a307-1e008c48bfd2",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "NotesAndFilesTabGridLayoutb42c0698"
					},
					"bindTo": "GbcEmail",
					"enabled": true
				},
				"parentName": "NotesAndFilesTabGridLayoutb42c0698",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "STRINGf7983463-beea-4741-9724-d1741743c119",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "NotesAndFilesTabGridLayoutb42c0698"
					},
					"bindTo": "GbcTelegram",
					"enabled": true
				},
				"parentName": "NotesAndFilesTabGridLayoutb42c0698",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "STRING63e4b80f-7750-47ed-bdff-9c6ef85c8d39",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "NotesAndFilesTabGridLayoutb42c0698"
					},
					"bindTo": "GbcMobilePhone",
					"enabled": true
				},
				"parentName": "NotesAndFilesTabGridLayoutb42c0698",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "GbcSchema5ac6dcd0Detaila48a2dc1",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "ActivityDetailInApplicant",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "NotesAndFilesTab",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "insert",
				"name": "Tabd600d188TabLabel",
				"values": {
					"caption": {
						"bindTo": "Resources.Strings.Tabd600d188TabLabelTabCaption"
					},
					"items": [],
					"order": 1
				},
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 1
			},
			{
				"operation": "merge",
				"name": "ESNTab",
				"values": {
					"order": 2
				}
			}
		]/**SCHEMA_DIFF*/
	};
});
